import uproot
import pandas as pd

root_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/mc16a/Nominal/mc16a.MGPy8_hh_bbyy_vbf_l1cvv0cv1.MxAODDetailedNoSkim.e8263_s3126_r9364_p4239_h026.root"

vars = ['HGamEventInfoAuxDyn.crossSectionBRfilterEff', 'HGamEventInfoAuxDyn.weight', 'HGamEventInfoAuxDyn.yybb_weight', 'HGamEventInfoAuxDyn.weightFJvt', 'EventInfoAux.eventNumber', 'HGamEventInfoAuxDyn.passCrackVetoCleaning']
#vars = ["HGamEventInfoAuxDyn.weight"]

for block in uproot.iterate(
    f"{root_path}:CollectionTree",
        vars,
        library="pd",
        #step_size="200 MB",
        step_size=5,
):
    print(block.to_string())
    break
# provided by Alkaid Cheng

import os
import pandas as pd
import numpy as np

basedir = "/eos/user/c/chlcheng/analysis/bbyy/VBF_optimization/preselected_h026_01_04_2022/arrays"
fname = os.path.join(basedir, "hh_bbyy_vbf_l1cvv1cv1.csv")
df = pd.read_csv(fname)

df_tightScore_HMass = df[df['ggF_cat'] // 1000 == 1 ]
df_looseScore_HMass = df[df['ggF_cat'] // 1000 == 2 ]
df_tightScore_LMass = df[df['ggF_cat'] // 1000 == 3 ]
df_looseScore_LMass = df[df['ggF_cat'] // 1000 == 4 ]

yield_tightScore_HMass = df_tightScore_HMass['weight'].sum()
yield_looseScore_HMass = df_looseScore_HMass['weight'].sum()
yield_tightScore_LMass = df_tightScore_LMass['weight'].sum()
yield_looseScore_LMass = df_looseScore_LMass['weight'].sum()

print("yield_looseScore_HMass =", yield_looseScore_HMass, df_looseScore_HMass['weight'].count())
print("yield_looseScore_LMass =", yield_looseScore_LMass, df_looseScore_LMass['weight'].count())
print("yield_tightScore_HMass =", yield_tightScore_HMass, df_tightScore_HMass['weight'].count())
print("yield_tightScore_LMass =", yield_tightScore_LMass, df_tightScore_LMass['weight'].count())

import pathlib
import sys

import pandas as pd
import psutil
import uproot
import ROOT

MB = 1024 * 1024
GB = MB * 1024

# setups
test_ntup_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/mc16a/Nominal/mc16a.MGH7_hh_bbyy_vbf_l0cvv1cv1.MxAODDetailedNoSkim.e7254_s3126_r9364_p4207_h026.root"
tree_name = "CollectionTree"

for block in uproot.iterate(
    f"{test_ntup_path}:{tree_name}",
    ["HGamEventInfoAuxDyn.isPassed", "HGamEventInfoAuxDyn.crossSectionBRfilterEff", "HGamEventInfoAuxDyn.weight", "HGamEventInfoAuxDyn.weightFJvt", "HGamEventInfoAuxDyn.yybb_weight", "HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77"],
    library="pd",
    step_size=5,
):
    #print(block.to_string())

    #df = block.loc[block["subentry"] == 0]
    #x = block.groupby(['entry'])["HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77"].sum()
    #df["n_obs"] = x

    print(block.to_string())
    #print(block["HGamEventInfoAuxDyn.isPassed"])
    #print(block.info())

    

    sub = block.xs(0, level='subentry')
    #sub["n_obs"] = block.groupby(['entry'])["HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77"].sum()
    sub["n_obs"] = getattr(block.groupby(['entry'])["HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77"], "sum")()
    print(sub.to_string())

    print("#" * 20)
    block.rename(columns={"HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77": "n_eff77"}, inplace=True)
    
    #block["valid"] = block.eval("n_eff77 > 0")
    #block["sum"] = block.groupby(['entry'])["valid"].transform("sum")
    
    block["n_jets"] = block.eval("n_eff77 > 0").groupby(['entry']).transform("sum")
    
    print(block.to_string())



    break

#in_file = ROOT.TFile.Open(test_ntup_path, "READ")
#tree = in_file.Get("CollectionTree")
#
#for entryNum in range (0 , tree . GetEntries ()):
#    m_yy = getattr(tree ,"HGamEventInfoAuxDyn.m_yy")
#    print("m_yy=", m_yy)
#    if entryNum == 10:
#        break
#
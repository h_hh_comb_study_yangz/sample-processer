import json
import yaml
import pathlib

# Term Translation
term_dict = {
    "tightScore_HMass": "SM_1",
    "looseScore_HMass": "SM_2",
    "tightScore_LMass": "BSM_1",
    "looseScore_LMass": "BSM_2",
}

# Get data to be compared with
json_path = pathlib.Path("../../../results/ref/yields_bbyy_chlcheng.json")
with open(json_path, "r") as f:
    ref = json.load(f)

# Get my data
yaml_path = pathlib.Path("../../../results/check_yield/bbyy_vbf.yaml")
with open(yaml_path, "r") as f:
    my = yaml.load(f, Loader=yaml.FullLoader)

for process, val in my.items():
    print("Checking process:", process)
    for region, val2 in val.items():
        my_yield = val2["all"]["yield"]
        ref_yield = ref[term_dict[region]][process]
        diff = abs(my_yield - ref_yield) / ref_yield * 100
        print(f"    {region} diff : {diff:.8f}%")
import logging
import pathlib
import re
from collections import defaultdict

import atlas_mpl_style as ampl
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import ROOT
import yaml
from scipy.interpolate import Rbf, RBFInterpolator, RectBivariateSpline, interp2d

# Ignore ROOT warnings
ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 6001;")
logger = logging.getLogger("h_hh_sp")


def scan_2d(config):
    jc = config.job
    ic = config.input
    sc = config.study
    input_dir = pathlib.Path(ic.input_dir)
    output_dir = pathlib.Path(jc.result_dir)
    output_dir.mkdir(exist_ok=True, parents=True)
    # Get reference level
    ref_path = input_dir / f"{sc.ref_file}.root"
    tf = ROOT.TFile(str(ref_path))
    ref_nll = None
    for entry in tf.Get(ic.tree_name):
        ref_nll = getattr(entry, sc.target_var)
        break
    # Get 2D data
    scan_files = True
    x_points, y_points = set(), set()
    # n_ll_list = []
    x_cor, y_cor, z_val = [], [], []
    x_cor_failed, y_cor_failed = [], []
    if sc.use_cache:
        cache_path = output_dir / f"{sc.output_file_name}.yaml"
        try:
            with cache_path.open("r") as f:
                cache_dict = yaml.load(f, Loader=yaml.FullLoader)
            scan_files = False
            logger.info(f"Loaded cache data at: {cache_path}")
        except:
            logger.error(
                f"Failed to load cache data at: {cache_path} as specified in config file. Continue scanning."
            )
    if scan_files:
        input_paths = [x for x in input_dir.glob("*.root") if x.is_file()]
        nll_dict = defaultdict(lambda: defaultdict(int))
        skiiped_files = []
        n_data = 0
        for p in input_paths:
            f = p.stem
            try:
                res = re.search(sc.para_re, f)
                p1 = float(res.group(1))
                p2 = float(res.group(2))
                # Get nll
                tf = ROOT.TFile(str(p))
                z = np.NAN
                for entry in tf.Get(ic.tree_name):
                    status = getattr(entry, "status")
                    if status == 0:
                        z = 2 * (getattr(entry, sc.target_var) - ref_nll)
                        if np.isnan(z):
                            skiiped_files.append((f, status))
                            x_cor_failed.append(p1)
                            y_cor_failed.append(p2)
                            break
                        nll_dict[p1][p2] = z
                        x_points.add(p1)
                        y_points.add(p2)
                        x_cor.append(p1)
                        y_cor.append(p2)
                        z_val.append(z)
                    else:
                        skiiped_files.append((f, status))
                        x_cor_failed.append(p1)
                        y_cor_failed.append(p2)
                    break
                logger.info(f"{f} processed, {p1} {p2} >> {z}")
                n_data += 1
            except AttributeError:
                skiiped_files.append((f, "AttributeError"))
        logger.info(f"{n_data} data points found.")
        if len(skiiped_files) > 0:
            logger.warning(
                f"{len(skiiped_files)} files skipped: , please confirm they are expected."
            )
            for f, s in skiiped_files:
                logger.warning(f"  >> {f} ({s})")
        # Save cache
        with open(output_dir / f"{sc.output_file_name}.yaml", "w") as f:
            plain_nll = {}
            for ky, val in nll_dict.items():
                plain_nll[ky] = dict(val)
            cache_dict = {
                "plain_nll": plain_nll,
                "x_points": list(x_points),
                "y_points": list(y_points),
                "x_cor": x_cor,
                "y_cor": y_cor,
                "z_val": z_val,
                "x_cor_failed": x_cor_failed,
                "y_cor_failed": y_cor_failed,
            }
            yaml.dump(cache_dict, f, default_flow_style=False)
    else:
        nll_dict = cache_dict["plain_nll"]
        x_points = cache_dict["x_points"]
        y_points = cache_dict["y_points"]
        x_cor = cache_dict["x_cor"]
        y_cor = cache_dict["y_cor"]
        z_val = cache_dict["z_val"]
        x_cor_failed = cache_dict["x_cor_failed"]
        y_cor_failed = cache_dict["y_cor_failed"]
    # Get 2D hist
    if 0 in z_val:
        print(f"found 0 in z_val")
    # Interpolate
    rbf_cfg = sc.rbf_cfg.get_config_dict()
    z, x, y, z_valid = interp_region(rbf_cfg, x_points, y_points, x_cor, y_cor, z_val)
    # Find best fit
    min_pos_x, min_pos_y = get_best_fit(x, y, z_valid)
    # Plot Contours
    fig, ax = plt.subplots()
    lvs = []
    lv_labels = []
    for lv_label, lv in sc.levels.items():
        lvs.append(lv)
        lv_labels.append(lv_label)
    cs = ax.contour(x, y, z, levels=lvs)
    if sc.show_best_fit:
        bf = ax.plot(min_pos_x, min_pos_y, **sc.best_fit_plot_args)
    if sc.extra_markers:
        for _, paras in sc.extra_markers.items():
            ax.plot(paras["x"], paras["y"], **paras["plot_args"])
    h1, _ = cs.legend_elements()
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles + h1, labels + lv_labels, loc=sc.legend_loc)
    ax.set_xlabel(sc.xlabel)
    ax.set_xlim(sc.xlim)
    ax.set_ylabel(sc.ylabel)
    ax.set_ylim(sc.ylim)
    fig.savefig(output_dir / f"{sc.output_file_name}.png")
    ax.plot(x_cor_failed, y_cor_failed, ".", color="black", ms=0.2)
    fig.savefig(output_dir / f"{sc.output_file_name}_with_invalid_points.png")
    # Check valid input points
    fig, ax = plt.subplots()
    ax.plot(x_cor, y_cor, ".", color="black", ms=0.2)
    ax.set_xlabel(sc.xlabel)
    ax.set_xlim(sc.xlim)
    ax.set_ylabel(sc.ylabel)
    ax.set_ylim(sc.ylim)
    ax.set_title("Valid input points")
    fig.savefig(output_dir / f"{sc.output_file_name}_valid_points.png")
    # Check invalid input points
    fig, ax = plt.subplots()
    ax.plot(x_cor_failed, y_cor_failed, ".", color="black", ms=0.2)
    ax.set_xlabel(sc.xlabel)
    ax.set_xlim(sc.xlim)
    ax.set_ylabel(sc.ylabel)
    ax.set_ylim(sc.ylim)
    ax.set_title("Invalid input points")
    fig.savefig(output_dir / f"{sc.output_file_name}_invalid_points.png")


def get_best_fit(x, y, z_valid):
    min_val = np.min(z_valid)
    min_r, min_c = np.unravel_index(np.argmin(z_valid), z_valid.shape)
    min_pos_x = x[min_r, min_c]
    min_pos_y = y[min_r, min_c]
    logger.info(f"min_val: {min_val} found at ({min_pos_x}, {min_pos_y})")
    return min_pos_x, min_pos_y


def interp_region(rbf_cfg, x_points, y_points, x_cor, y_cor, z_val):
    # Interpolate points
    logger.info(f"Performing 2D interpolation with Rbf method.")
    # ff = interp2d(x_cor, y_cor, z_val, kind="linear")
    # ff = Rbf(x_cor, y_cor, z_val, **rbf_cfg)
    points = [(x, y) for x, y in zip(x_cor, y_cor)]
    ff = RBFInterpolator(points, z_val, **rbf_cfg)

    x = sorted(list(x_points))
    y = sorted(list(y_points))
    x, y = np.meshgrid(x, y)
    z = np.zeros(x.shape)
    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            z[i, j] = ff([[x[i, j], y[i, j]]])
    # z = np.vectorize(ff)(x, y)
    z_valid = np.ma.MaskedArray(z, z <= 0)
    return z, x, y, z_valid


def scan_2d_comb(config):
    jc = config.job
    ic = config.input
    sc = config.study
    output_dir = pathlib.Path(jc.result_dir)
    output_dir.mkdir(exist_ok=True, parents=True)
    # Get inputs
    data = {}
    for process, f_path in ic.processes.items():
        logger.info(f"Loading {process} data from {f_path}")
        with pathlib.Path(f_path).open("r") as f:
            data[process] = yaml.load(f, Loader=yaml.FullLoader)
    # Plot contours
    fig, ax = plt.subplots()
    # Loop over processes
    lg_hds_process = []
    lg_lbs_process = []
    lg_hds_best_fit = []
    lg_lbs_best_fit = []
    for process, d in data.items():
        logger.info(f"Processing: {process}")
        p_color = sc.processes_colors[process]
        p_process = sc.processes_labels[process]
        lg_patch = mlines.Line2D([], [], color=p_color, label=p_process)
        lg_hds_process.append(lg_patch)
        lg_lbs_process.append(p_process)
        # Get x, y, z
        x_points = d["x_points"]
        y_points = d["y_points"]
        x_cor = d["x_cor"]
        y_cor = d["y_cor"]
        z_val = d["z_val"]
        # Get 2D hist
        if 0 in z_val:
            print(f"found 0 in z_val")
        # Interpolate
        rbf_cfg = sc.processes_rbf_cfg[process].get_config_dict()
        z, x, y, z_valid = interp_region(
            rbf_cfg, x_points, y_points, x_cor, y_cor, z_val
        )
        # Find best fit
        min_pos_x, min_pos_y = get_best_fit(x, y, z_valid)
        # Plot contours
        lvs = []
        lv_labels = []
        lv_styles = []
        for lv_label, lv in sc.levels.items():
            lvs.append(lv)
            lv_labels.append(lv_label)
            lv_styles.append(sc.levels_styles[lv_label])
        cs = ax.contour(x, y, z, levels=lvs, linestyles=lv_styles, colors=p_color)
        if sc.show_best_fit:
            plt_cfgs = sc.best_fit_plot_args.get_config_dict()
            # Override color with process color
            plt_cfgs["color"] = p_color
            plt_cfgs["markeredgecolor"] = p_color
            plt_cfgs["markerfacecolor"] = p_color
            bf = ax.plot(min_pos_x, min_pos_y, **plt_cfgs)
            lg_hds_best_fit.append(bf[0])
            lg_lbs_best_fit.append(f"Best Fit ({p_process})")
    # Plot extra markers
    lg_hds_ex_mk = []
    lg_lbs_ex_mk = []
    if sc.extra_markers:
        for name, paras in sc.extra_markers.items():
            (mk,) = ax.plot(paras["x"], paras["y"], **paras["plot_args"])
            lg_hds_ex_mk.append(mk)
            if "label" in paras:
                lg_lbs_ex_mk.append(paras["label"])
            else:
                lg_lbs_ex_mk.append(name)
    # Prepare legend
    lg_hds_lv = []
    lg_lbs_lv = []
    for lv_label, lv in sc.levels.items():
        lg_patch = mlines.Line2D(
            [], [], color="black", linestyle=sc.levels_styles[lv_label], label=lv_label
        )
        lg_hds_lv.append(lg_patch)
        lg_lbs_lv.append(lv_label)
    place_holder = mlines.Line2D([], [], color="none", label="")
    lg_hds = lg_hds_ex_mk + lg_hds_best_fit + lg_hds_lv 
    lg_hds += [place_holder] + lg_hds_process
    lg_lbs = lg_lbs_ex_mk + lg_lbs_best_fit + lg_lbs_lv + [""] + lg_lbs_process
    ax.legend(handles=lg_hds, labels=lg_lbs, loc=sc.legend_loc)
    # ATLAS label
    if sc.plot_atlas_label:
        ampl.plot.draw_atlas_label(
            0.05,
            0.95,
            ax=ax,
            **(sc.atlas_label.get_config_dict()),
        )
    # Adjust plot
    ax.set_xlabel(sc.xlabel)
    ax.set_xlim(sc.xlim)
    ax.set_ylabel(sc.ylabel)
    ax.set_ylim(sc.ylim)
    # Save plot
    fig.savefig(output_dir / f"{sc.output_file_name}.png")

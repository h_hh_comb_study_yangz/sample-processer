import logging
import pathlib
from typing import Hashable, Optional, Tuple, Union

import pandas as pd
import ROOT
import uproot

# Ignore ROOT warnings
ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 6001;")
logger = logging.getLogger("h_hh_sp")


def check_yields(
    input_dir: Union[str, pathlib.Path],
    files: list,
    tree_name: str,
    tree_level: int,
    weight_var: str,
    cut: str,
    vars: list,
    hist_name: str = "",
    hist_vars: Hashable = {},
    hist_runtime_vars: Hashable = {},
    total_weight_var: Optional[str] = None,
    groupby: list = [],
    grouped_vars: Hashable = {},
    grouped_methods: Hashable = {},
    runtime_vars: Hashable = {},
) -> float:
    """Checks ntuples yields."""
    # Formatting variable names for pandas.eval
    var_to_pdvar, pdvar_to_var = pd_rename_var(
        vars
        + groupby
        + list(grouped_vars.keys())
        + list(runtime_vars.keys())
        + [weight_var]
    )
    _vars = [var_to_pdvar[var] for var in vars]
    _groupby = [var_to_pdvar[var] for var in groupby]
    _grouped_vars = {
        var_to_pdvar[ky]: pd_rename_expr(val, var_to_pdvar)
        for ky, val in grouped_vars.items()
    }
    _grouped_methods = {var_to_pdvar[ky]: val for ky, val in grouped_methods.items()}
    _runtime_vars = {
        var_to_pdvar[ky]: pd_rename_expr(val, var_to_pdvar)
        for ky, val in runtime_vars.items()
    }
    _weight_var = var_to_pdvar[weight_var]
    _cut = pd_rename_expr(cut, var_to_pdvar)
    # Get yields
    cur_wts = 0
    cur_count = 0
    total_wt = 0
    for file in files:
        file_path = pathlib.Path(input_dir).joinpath(file)
        # Get hist_vars
        tf = ROOT.TFile(str(file_path))
        hist = tf.Get(hist_name)
        hv_dict = {}
        for ky, val in hist_vars.items():
            hv_dict[ky] = [hist.GetBinContent(val)]
        hist_pd = pd.DataFrame(hv_dict)
        for ky, val in hist_runtime_vars.items():
            hv_dict[ky] = float(hist_pd.eval(val))
        # print("#### hv_dict:", hv_dict)
        if total_weight_var:
            total_wt += hv_dict[total_weight_var]
        # Loop over entries
        for block in uproot.iterate(
            f"{file_path}:{tree_name}",
            vars,
            library="pd",
            step_size="200 MB",
            # step_size=50,
        ):
            logger.debug(f"Processing file: {file_path}")
            block.rename(columns=var_to_pdvar, inplace=True)
            # Get flat inputs
            if tree_level == 1:
                if _grouped_vars:
                    logger.warning("Grouped variables are not supported for tree level 1!")
                block_1d = block
            elif tree_level == 2:
                if _grouped_vars:
                    for _var, _obj in _grouped_vars.items():
                        block[_var] = (
                            block.eval(_obj)
                            .groupby(_groupby)
                            .transform(_grouped_methods[_var])
                        )
                block_1d = block.xs(0, level="subentry")
            else:
                logger.error(f"Tree level {tree_level} not supported.")
                break
            # Runtime variables
            for _var, _expr in _runtime_vars.items():
                block_1d[_var] = block_1d.eval(_expr)
            # apply cut
            block_1d.query(_cut, inplace=True)
            # Add yields
            cur_wts += block_1d[_weight_var].sum()
            cur_count += block_1d.shape[0]
    return cur_wts, cur_count, total_wt


def pd_to_pdvar(var: str) -> str:
    """Renames variable to be used normally in pandas.eval."""
    return var.replace(".", "___DOT___")


def pd_rename_var(vars: list) -> Tuple[Hashable, Hashable]:
    """Gets dict for renameing variables to be used normally in pandas.eval."""
    var_to_pdvar = {}
    pdvar_to_var = {}
    for var in vars:
        pdvar = var.replace(".", "___DOT___")
        var_to_pdvar[var] = pdvar
        pdvar_to_var[pdvar] = var
    return var_to_pdvar, pdvar_to_var


def pd_rename_expr(expr: str, rename_dict: dict) -> str:
    """Renames expression to be used normally in pandas.eval."""
    for ky, val in rename_dict.items():
        expr = expr.replace(ky, val)
    return expr

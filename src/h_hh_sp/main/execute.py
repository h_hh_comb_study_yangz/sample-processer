import argparse
import logging
import time

import h_hh_sp
from h_hh_sp.main.processor import SampleProcessor

logger = logging.getLogger("h_hh_sp")

def execute():
    """Enterpoint of h_hh_sp command"""
    parser = argparse.ArgumentParser()
    parser.add_argument("yaml_configs", nargs="*", action="store")
    parser.add_argument(
        "-d",
        "--debug",
        required=False,
        help="run in debug mode",
        action="store_true",
    )
    parser.add_argument(
        "-t",
        "--time",
        required=False,
        help="show log time",
        action="store_true",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        required=False,
        help="verbose debug infomation",
        action="store_true",
    )
    args = parser.parse_args()

    # check input
    if not args.yaml_configs:
        parser.print_help()
        exit()
    else:
        # set debug level
        logging_format = "%(levelname)-8s | %(message)s"
        if args.verbose:
            logging_format += "| %(filename)s | #%(lineno)d"
        if args.time:
            logging_format = "%(asctime)s.%(msecs)03d | " + logging_format
        if args.debug:  # DEBUG, INFO, WARNING, ERROR, CRITICAL
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)
        logging.basicConfig(
            format=logging_format,
            datefmt="%Y-%m-%d:%H:%M:%S",
        )
        # execute jobs
        for yaml_cfg in args.yaml_configs:
            logger.info("#" * 80)
            logger.info(f"Executing: {yaml_cfg}")
            time_start_stamp = time.perf_counter()

            # Execute job
            sp = SampleProcessor(yaml_cfg)
            if args.debug:
                sp.get_config().print()
            sp.run()

            time_end_stamp = time.perf_counter()
            time_cost = time_end_stamp - time_start_stamp
            logger.info(
                f"Time consumed: {time.strftime('%H:%M:%S', time.gmtime(time_cost))}"
            )
            logger.info("Done!")


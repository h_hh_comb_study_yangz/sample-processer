import datetime
import logging
import pathlib

import atlas_mpl_style as ampl
import yaml
from h_hh_sp.common import config, helper
from h_hh_sp.study import llh_scan, yield_check

logger = logging.getLogger("h_hh_sp")


class SampleProcessor(object):
    """Process Ntuples for H+HH combination studies."""

    def __init__(self, yaml_path) -> None:
        """Initialize processor"""
        self.config = None
        self.load_config(yaml_path)
        ampl.use_atlas_style(usetex=False)

    def get_config(self) -> config.Config:
        """Returns config"""
        return self.config

    def load_config(self, yaml_path) -> None:
        """Load yaml config"""
        cfg_path = helper.get_valid_cfg_path(yaml_path)
        yaml_dict = config.load_yaml_dict(cfg_path)
        temp_cfg = config.Config(yaml_dict)
        # Check whether need to import other (default) ini file first
        if hasattr(temp_cfg, "config"):
            if hasattr(temp_cfg.config, "include"):
                import_ini_path_list = temp_cfg.config.include
                if import_ini_path_list:
                    for include_path in import_ini_path_list:
                        self.load_config(include_path)
                        logger.info(f"Included config: {include_path}")
        if self.config is not None:
            self.config.update(yaml_dict)
        else:
            self.config = temp_cfg
        # Setup runtime config (dynamic)
        self.config.run = config.Config_Section({}, section_name="run")
        datestr = datetime.date.today().strftime("%Y-%m-%d")
        self.config.run.datestr = datestr
        self.config.run.config_collected = True

    def run(self) -> None:
        """Run defined job"""
        job = self.config.job.job_type
        if job == "get_yield":
            self.get_yield()
        elif job == "llh_scan_2d":
            llh_scan.scan_2d(self.config)
        elif job == "llh_scan_2d_comb":
            llh_scan.scan_2d_comb(self.config)
        else:
            logger.error(f"Unknown job type: {job}")

    def get_yield(self) -> None:
        """Get yield from Ntuples"""
        jc = self.config.job
        ic = self.config.input
        cc = self.config.cut
        result = {}
        input_dir = pathlib.Path(ic.input_dir)
        for process, process_map in ic.recipes.items():
            logger.info(f"YIELD: {process}")
            result[process] = {}
            hist_name = ic.hist_names[process]
            es = 1
            if process in ic.extra_scale.keys():
                es = ic.extra_scale[process]
            for category, customize_cut in cc.cut_book.items():
                logger.info(f"    {category}")
                result[process][category] = {}
                total_wts, total_count, sum_of_wts = 0, 0, 0
                total_yields = 0
                for camp, files in process_map.items():
                    if camp in customize_cut:
                        cut = cc.cut_def[customize_cut[camp]]
                    else:
                        cut = cc.cut_def[category]
                    cur_wts, cur_count, cur_sum_of_wts = yield_check.check_yields(
                        input_dir=input_dir / ic.input_subdir[camp],
                        files=files,
                        tree_name=ic.tree_name,
                        tree_level=ic.tree_level,
                        weight_var=ic.weight_var,
                        cut=cut,
                        vars=ic.vars,
                        hist_name=hist_name,
                        hist_vars=ic.hist_vars,
                        hist_runtime_vars=ic.hist_runtime_vars,
                        total_weight_var=ic.total_weight_var,
                        groupby=ic.groupby,
                        grouped_vars=ic.grouped_vars,
                        grouped_methods=ic.grouped_methods,
                        runtime_vars=ic.runtime_vars,
                    )
                    total_wts += cur_wts
                    total_count += cur_count
                    sum_of_wts += cur_sum_of_wts
                    # Show current yield
                    cur_yields = cur_wts * ic.lumi[camp] / cur_sum_of_wts * es
                    logger.info(f"        {camp} : {cur_yields} ({cur_count})")
                    result[process][category][camp] = {
                        "yield": float(cur_yields),
                        "count": int(cur_count),
                    }
                    total_yields += cur_yields
                # Show total yield
                logger.info(f"        all : {total_yields} ({total_count})")
                result[process][category]["all"] = {
                    "yield": float(total_yields),
                    "count": int(total_count),
                }
        # Save result
        output_dir = pathlib.Path(jc.result_dir)
        output_dir.mkdir(parents=True, exist_ok=True)
        output_path = output_dir / f"{jc.result_file}.yaml"
        with open(output_path, "w") as f:
            yaml.dump(result, f, default_flow_style=False, indent=4)
            logger.info(f"Saved result to {output_path}")

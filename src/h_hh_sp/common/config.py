import collections
import copy
import logging

import pandas as pd
import yaml
from h_hh_sp.common.config_default import *

pd.options.mode.chained_assignment = None  # default='warn'
logger = logging.getLogger("h_hh_sp")

# some configs must have the value to make the code run normally, so the default
# configs are prepared for those unspecified but missing settings


class Config(object):
    """Helper class to handle job configs"""

    def __init__(self, config_dict: dict) -> None:
        self._config_dict = copy.deepcopy(config_dict)
        self.update(DEFAULT_CFG)
        self.update(config_dict)

    def __getitem__(self, item):
        return getattr(self, item)

    def __iter__(self):
        for key, val in self.items():
            yield (key, val)

    def get_config_dict(self) -> dict:
        """ Returns config in dict format """
        return self._config_dict

    def update(self, config: dict) -> None:
        """Updates configs with given config dict, overwrite if exists

        Args:
            config: two level dictionary of configs
        """
        merge_dict = copy.deepcopy(config)
        dict_merge(self._config_dict, merge_dict)
        for key, value in config.items():
            if type(value) is dict:
                if key in self.__dict__.keys() and key != "_config_dict":
                    getattr(self, key).update(value)
                else:
                    setattr(self, key, Config_Section(value, section_name=key))
            elif value is None:
                pass
            else:
                logger.critical(
                    f"Expect section {key} has dict type value or None, please check the input."
                )
                exit(1)

    def print(self) -> None:
        """Shows all configs"""
        logger.info("")
        logger.info("Config details " + ">" * 80)
        for key, value in self.__dict__.items():
            if key != "_config_dict":
                logger.info(f"[{key}]")
                value.print()
        logger.info("Config ends " + "<" * 83)
        logger.info("")

    def keys(self):
        return self.get_config_dict().keys()

    def values(self):
        return self.get_config_dict().values()

    def items(self):
        return self.get_config_dict().items()


class Config_Section(object):
    """Helper class to handle job configs in a section"""

    def __init__(self, section_config_dict: dict, section_name: str) -> None:
        self._section_name = section_name
        self._config_dict = copy.deepcopy(section_config_dict)
        for key, value in section_config_dict.items():
            if type(value) is dict:
                setattr(
                    self,
                    key,
                    Config_Section(value, section_name=f"{self._section_name}.{key}"),
                )
            else:
                setattr(self, key, value)

    def __getattr__(self, item):
        """Called when an attribute lookup has not found the attribute in the usual places"""
        if self._section_name in DEFAULT_CFG:
            # ref_config_section = Config_Section(
            #    DEFAULT_CFG[self._section_name], section_name=self._section_name
            # )
            ref_config_section = DEFAULT_CFG[self._section_name]
            if item in ref_config_section:
                return ref_config_section[item]
        return None

    def __getitem__(self, item):
        return getattr(self, item)

    def __iter__(self):
        for key, val in self.items():
            yield (key, val)

    def get_config_dict(self) -> dict:
        """ Returns config in dict format """
        return self._config_dict

    def update(self, cfg_dict: dict) -> None:
        """Updates the section config dict with new dict, overwrite if exists

        Args:
            cfg_dict: new section config dict for update
        """
        merge_dict = copy.deepcopy(cfg_dict)
        dict_merge(self._config_dict, merge_dict)
        for key, value in cfg_dict.items():
            if type(value) is dict:
                if key in self.__dict__.keys() and key != "_config_dict":
                    getattr(self, key).update(value)
                else:
                    setattr(
                        self,
                        key,
                        Config_Section(value, section_name=self._section_name),
                    )
            else:
                setattr(self, key, value)

    def print(self, tabs: int = 0) -> None:
        """ Shows all section configs """
        for key, value in self.__dict__.items():
            if key != "_config_dict":
                if isinstance(value, Config_Section):
                    logger.info(f"{' '*4*tabs}    {key} :")
                    value.print(tabs=tabs + 1)
                elif isinstance(value, list):
                    logger.info(f"{' '*4*tabs}    {key} :")
                    for ele in value:
                        logger.info(f"{' '*4*tabs}        - {ele}")
                else:
                    logger.info(f"{' '*4*tabs}    {key} : {value}")

    def keys(self):
        return self.get_config_dict().keys()

    def values(self):
        return self.get_config_dict().values()

    def items(self):
        return self.get_config_dict().items()


def dict_merge(my_dict: dict, merge_dict: dict):
    """ Recursive dict merge """
    merge_dict = copy.deepcopy(merge_dict)
    for key in merge_dict.keys():
        if (
            key in my_dict
            and isinstance(my_dict[key], dict)
            and isinstance(merge_dict[key], collections.Mapping)
        ):
            dict_merge(my_dict[key], merge_dict[key])
        else:
            my_dict[key] = merge_dict[key]


def load_yaml_dict(yaml_path: str) -> dict:
    try:
        yaml_file = open(yaml_path, "r")
        return yaml.load(yaml_file, Loader=yaml.FullLoader)
    except:
        logger.critical(f"Can't open yaml config: {yaml_path}")
        exit(1)

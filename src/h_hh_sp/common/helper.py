from asyncio.log import logger
import pathlib
from typing import Union
import logging
import h_hh_sp

logger = logging.getLogger("h_hh_sp")

def get_valid_cfg_path(path: Union[str, pathlib.Path]) -> pathlib.Path:
    """Finds valid path for cfg file in /share folder.

    Args:
        path: path to the job config file

    Returns:
        pathlib.Path: valid job config path

    If path is already valid:
      Nothing will be done and original path will be returned.
    If path is not valid:
      Try to add share folder path before to see whether we can get a valid path.
      Otherwise, show error to ask configuration correction.

    """
    # check if path is valid
    path = pathlib.Path(path)
    if path.is_file():
        return path
    # try add share folder prefix
    share_dir = pathlib.Path(h_hh_sp.__file__).parents[1]
    logger.debug(f"Get /share dir: {share_dir} as prefix")
    cfg_path = share_dir.joinpath(path)
    if cfg_path.is_file():
        return cfg_path
    elif cfg_path.is_dir():
        logger.critical(
            f"Expect a config file path but get directory path: {cfg_path}, please check the config file."
        )
        exit(1)
    else:
        logger.critical(
            f"No valid config file found at path {cfg_path}, please check the config file."
        )
        exit(1)

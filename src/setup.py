import setuptools

setuptools.setup(
    name="h_hh_sp",
    version="0.1.0",
    auther="Zhe Yang",
    auther_email="starprecursor@gmail.com",
    description="Process samples for H+HH combination tasks",
    url="https://gitlab.cern.ch/h_hh_comb_study_yangz/sample-processer",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
    install_requires=[
        "numpy>=1.18.0",
        "pandas>=1.2.0",
        "matplotlib>=3.2.0",
        "uproot>=4.0.0",
        "scipy>=1.7",
        "PyYAML",
        "black",
        "atlas_mpl_style",
    ],
    entry_points={
        "console_scripts": ["h_hh_sp=h_hh_sp.main.execute:execute",],
    },
)

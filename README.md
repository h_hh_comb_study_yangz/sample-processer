# Sample Processer

Process samples for H+HH combination tasks

## **Environment**

- python version: **python 3.8 or higher is required**

- for atint users, setup proper eos access

    ```bash
    kinit
    eosfusebind
    ```

### **First setup**

use [Anaconda](https://docs.anaconda.com/anaconda/install/) to set up a python 3.8+ environment

```bash
conda create -n h_hh_sp python=3.8
conda activate h_hh_sp
conda install -c conda-forge root=6.24.0 
```

### **Recurrent setup**

```bash
conda activate h_hh_sp
```

### **Exit environment**

```bash
conda deactivate
```

## **Installation**

Make sure Git properly setup and clone the repository to local

```bash
git clone --recursive ssh://git@gitlab.cern.ch:7999/h_hh_comb_study_yangz/sample-processer.git
```

Install with pip (use "-e" option to install in editable mode for development)

```bash
cd src
pip install .
```

Or "make" command:

```bash
make
```

## **Example Run**

Command usage

```bash
usage: h_hh_sp [-h] [-d] [-t] [-v] [yaml_configs [yaml_configs ...]]

positional arguments:
  yaml_configs

optional arguments:
  -h, --help     show this help message and exit
  -d, --debug    run in debug mode
  -t, --time     show log time
  -v, --verbose  verbose debug infomation

```

example

```bash
h_hh_sp share/config/bbyy/job/test/check_yield.yaml
```
